import { createMuiTheme } from '@material-ui/core'
import '../App.css'
export const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#7d259f'
    },
    secondary: {
      main: '#e3e4e8'
    }
  },
  typography: {
    fontFamily: 'Poppins'
  },
  overrides: {
    MuiButton: {
      text: {
        background: '#f2eef4',
        border: '1px solid #7d259f',
        padding: '0 10px',
        borderRadius: '5px'
      }
    }
  }
})
