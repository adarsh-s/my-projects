import React from "react";
import "./App.css";
import ModalNotification from "Components/ModalComponent";
import UserIcon from "Components/UserIconComponent";
import Badge from './Components/BadgeComponent/index'
import ButtonVal from './Components/ButtonComponent';
import TableLoader from "Components/LoaderComponent";

function App() {
  return (
    <div className="App">
      <ModalNotification />
      <br />
      <UserIcon value={"TMH"} />   
      <ButtonVal>FILTER</ButtonVal>
      <TableLoader/>
    </div>
  );
}

export default App;
