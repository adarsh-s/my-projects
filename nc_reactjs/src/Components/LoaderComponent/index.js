import React from "react";
import Skeleton from "react-loading-skeleton";
import TableStyle from "../ModalComponent/tableNotificationLogStyles";
const TableLoader = (props) => {
  return (
    <TableStyle>
      <table>
        <thead>
          <tr>
          <th><Skeleton height={15} width={50} /></th>
          <th><Skeleton height={15} width={50}/></th>
          <th><Skeleton height={15} width={50}/></th>
          <th><Skeleton height={15} width={50}/></th>
          </tr>
        </thead>
        <tbody >
        <tr>
             <td><Skeleton/></td>
             <td><Skeleton/></td>
             <td><Skeleton/></td>
             <td><Skeleton/></td>
         </tr>
         <tr>
             <td><Skeleton/></td>
             <td><Skeleton/></td>
             <td><Skeleton/></td>
             <td><Skeleton/></td>
         </tr>
        </tbody>
      </table>
    </TableStyle>
  );
};

export default TableLoader;
