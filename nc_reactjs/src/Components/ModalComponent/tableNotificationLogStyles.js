import styles from 'styled-components';

const TableStyle=styles.div`
text-align:center;
table { 
    width: 100%; 
    background-color: #ffffff;
    border-collapse: collapse; 
    paddingLeft:0,
    paddingRight:0,
    }
  th {  
    font-size: 14px;
    font-weight: 500;
 
  }
  td, th { 
    font-size: 12px;
    padding: 10px; 
    text-align: left; 
    border-bottom: 1px solid #e3e4e8;
    border-top: 1px solid #e3e4e8;
    font-family:'Poppins'
  }
  tr{
    height: 51px;
  }
  td{
    color:#8e93a5;
    text-align: left;
  }
  @media (max-width: 768px) {
    td, th { 
      font-size: 10px;
      padding: 5px; 
      text-align: left; 
      border-bottom: 1px solid #e3e4e8;
      border-top: 1px solid #e3e4e8;
      font-family:'Poppins'
    }
    tr{
      height: 51px;
    }
    td{
      color:#8e93a5;
    }
  }
`
export default TableStyle;
