import React from "react";
import TableStyle from "./tableNotificationLogStyles";
import { useTable } from "react-table";

const TableNotificationLog = ({ columns, data }) => {
  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    rows,
    prepareRow
  } = useTable({
    columns,
    data
  });
  return (
    <TableStyle>
      <table {...getTableProps()}>
        <thead>
          {headerGroups.map(headerGroup => (
            <tr {...headerGroup.getHeaderGroupProps()}>
              {headerGroup.headers.map(column => (
                <th {...column.getHeaderProps()}>{column.render("Header")}</th>
              ))}
              <th> Notified: Times </th>
            </tr>
          ))}
        </thead>
        <tbody {...getTableBodyProps()}>
          {rows.map((row, i) => {
            prepareRow(row);
            return (
              <tr {...row.getRowProps()}>
                {row.cells.map(cell => {
                  return (
                    <td {...cell.getCellProps()}>{cell.render("Cell")}</td>
                  );
                })}
                <td style={{textAlign:"left",color:"#7d259f"}} onClick={()=>{window.location.href = `mailto:`}}>EMAIL</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </TableStyle>
  );
};

export default TableNotificationLog;
