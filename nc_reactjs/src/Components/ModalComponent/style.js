import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(theme => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    textAlign: "center",
    minWidth: "50%"
  },
  paper: {
    backgroundColor: "#f4f7fc",
    borderRadius: 5,
    border: "solid 1px #e3e4e8",
    width: "50%"
  },
  root: {
    margin: 25,
    borderRadius: 5,
    position: "relative"
  },
  typography: {
    fontFamily: "Poppins",
    fontSize: "16px",
    fontWeight: 600,
    fontStretch: "normal",
    fontStyle: "normal",
    lineHeight: "normal",
    color: "#401a4f",
    float: "left",
    padding: "15px"
  },
  close: {
    float: "right",
    width: 20,
    height: 15.5,
    color: "#8e93a5"
  },
  span: {
    fontFamily: "Poppins",
    fontSize: "12px",
    fontWeight: "600",
    fontStretch: " normal",
    fontStyle: " normal",
    lineHeight: "0.75",
    letterSpacing: "normal",
    color: "#7d259f"
  }
}));
export default useStyles;
