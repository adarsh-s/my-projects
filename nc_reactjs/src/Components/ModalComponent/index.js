import React, { useState, useEffect } from "react";
import axios from "axios";
import { Modal, Card, Typography } from "@material-ui/core";
import CloseIcon from "@material-ui/icons/Close";
import useStyles from "./style";
import TableNotificationLog from "./tableNotificationLog";
import TableLoader from "Components/LoaderComponent";
import Button from "../ButtonComponent/index";

const ModalNotification = () => {
  const [open, setOpen] = React.useState(false);
  const columns = [
    {
      Header: "Date",
      accessor: "Date"
    },
    {
      Header: "Time",
      accessor: "Time"
    },
    {
      Header: "Recipient",
      accessor: "Recipient"
    }
  ];
  const [data, setData] = useState([]);

  useEffect(() => {
    (async () => {
      const result = await axios(
        "http://5e660e9a2aea440016afb861.mockapi.io/notificationlog"
      );
      setData(result.data);
    })();
  }, []);
  const classes = useStyles();

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };
  return (
    <div>
      <span onClick={handleOpen} className={classes.span}>
         History
      </span>
      <Modal open={open} onClose={handleClose} className={classes.modal}>
        <div className={classes.paper}>
          <CloseIcon className={classes.close} onClick={handleClose} />
          <Card className={classes.root}>
            <Typography className={classes.typography}>
              NOTIFICATION LOG
            </Typography>
            <Button position={"right"}>EMAIL ALL RECIPIENT</Button>
            {!data ? (
              <TableLoader rows={columns} />
            ) : (
              <TableNotificationLog columns={columns} data={data} />
            )}
          </Card>
        </div>
      </Modal>
    </div>
  );
};

export default ModalNotification;
