import React from "react";
import useStyles from './style';

const UserIcon = props => {
    const classes=useStyles();
  return (
    <div className={classes.icon}>
      <span className={classes.circle}>{props.value}</span>
    </div>
  );
};

export default UserIcon;
