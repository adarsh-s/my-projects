import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(theme => ({  
    circle:{
        fontSize:'8px',
        fontWeight: '600',
        fontStretch: 'normal',
        fontStyle: 'normal',
        textAlign:'center',
        lineHeight: 'normal',
        color:'#ffffff'
    },
    icon:{
        textAlign:'center',
        backgroundColor: '#404b69',
        borderRadius:'50%',
        width:'26px',
        height:'24px',
    }

}));
export default useStyles;
