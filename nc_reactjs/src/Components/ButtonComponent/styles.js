import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(theme => ({
    btn:{
        borderRadius: '5px',
        border: 'solid 1px #7d259f',
        backgroundColor:'#f2eef4',
        fontSize:"10px",
        fontFamily:'poppins',
        fontWeight: '600',
        fontStretch: 'normal',
        fontStyle: 'normal',
        lineHeight: 'normal',
        letterSpacing:'0.18px',
        textAlign: 'center',
        padding:'5px 15px',
        color: '#7d259f',
        margin:'5px',
        ['@media (max-width:768px)'] : {
            fontWeight: '00',
            fontStretch: 'normal',
            fontStyle: 'normal',
            lineHeight: 'normal',
            fontSize:'8px',
            color: '#7d259f',
            padding:'5px 13px',
            margin:'5px',
            
          }
    }
  }));
  export default useStyles;
  