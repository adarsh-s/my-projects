import React from 'react'
import useStyles from  "./styles.js"
import Button from '@material-ui/core/Button';

const ButtonVal = ({
  children,
  position,
  icon
}) => {
  const classes=useStyles();
  return (
    <Button className={classes.btn} style={{float:position}}  >
      {children}
    </Button>
  )
};

export default ButtonVal