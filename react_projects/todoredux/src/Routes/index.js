import ListTable from 'Components/TableDataComponent';
import AddTask from 'Components/FormComponent';
const routes = [
    {
      path: "/",
      exact: true,
      component: ListTable
    },
    {
      path: "/AddTask",
      component: AddTask
    },
  ]

export default routes;