import axios from 'axios'

export function get (url, payload = {}, headers={}) {
  return axios.get(url, payload, headers)
}

export function put (url, payload = {}, headers={}) {
  return axios.put(url, payload, headers)
}

export function post (url, payload = {}, headers={}) {
  return axios.post(url, payload, headers)
}

export function deleteData (url, payload = {}, headers={}) {
  return axios.delete(url, payload, headers)
}