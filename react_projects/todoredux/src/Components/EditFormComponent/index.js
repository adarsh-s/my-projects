import React from 'react';
import EditForm from 'Views/TodoView/Containers/editForm'

export default function EditFormComponent(props) {
  return(
    <div>
      <EditForm id={props.id} title={props.title} desc={props.desc}/>
    </div>
  );
  }