/* eslint-disable react/prop-types */
import React from "react";
import clsx from "clsx";
import SearchIcon from '@material-ui/icons/Search'
import InputBase from '@material-ui/core/InputBase'
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import styles from "./styles";
import { searchData } from 'Views/TodoView/Actions/searchAction';
import { connect } from 'react-redux'

const NavApp = props => {
  const classes = styles();
  const filterData = (event) => {
    props.dispatch(searchData(event.target.value))
  }
  return (
    <AppBar>
      <Toolbar>
        <IconButton
          color="inherit"
          aria-label="open drawer"
          onClick={props.openhandler}
          edge="start"
          className={clsx(classes.menuButton, props.openside && classes.hide)}
        >
          <MenuIcon />
        </IconButton>
        <Typography variant="h6" noWrap>
          TODO App
          
        </Typography>
        <div className={classes.rootsearch}>
      <div className={classes.search}>
        <div className={classes.searchIcon}>
          <SearchIcon />
        </div>
        <InputBase
          placeholder="Search…"
          onChange={(event) => filterData(event)}
          classes={{
            root: classes.inputRoot,
            input: classes.inputInput
          }}
          inputProps={{ 'aria-label': 'search' }}
        />
      </div>
    </div>
      </Toolbar>

    </AppBar>
  );
};
export default connect()(NavApp);
