import React from "react";
import Drawer from "@material-ui/core/Drawer";
import clsx from "clsx";
import { useTheme } from "@material-ui/core/styles";
import CssBaseline from "@material-ui/core/CssBaseline";
import Divider from "@material-ui/core/Divider";
import IconButton from "@material-ui/core/IconButton";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import { BrowserRouter as Router } from "react-router-dom";
import styles from "Components/DrawerComponent/styles";
import AppBar from "Components/AppBarComponent";
import Listitem from 'Components/NavListComponent';
import SwitchComponent from "Components/SwitchComponent";
import routes from 'Routes';
export default function NavDrawer(props) {
  const classes = styles();
  const theme = useTheme();
  const [open, setOpen] = React.useState(false);
  const handleDrawerOpen = () => {
    setOpen(true);
  };
  const handleDrawerClose = () => {
    setOpen(false);
  };
  return (
    <div className={classes.r1}>
      <CssBaseline />
      <AppBar
        openhandler={handleDrawerOpen}
        sidebar={open}
        changed={props.changed}
      />
      <Router>
        <Drawer
          className={classes.drawer}
          variant="persistent"
          anchor="left"
          open={open}
          classes={{ paper: classes.drawerPaper }}
        >
          <div className={classes.drawerHeader}>
            <IconButton onClick={handleDrawerClose}>
              {theme.direction === "ltr" ? (
                <ChevronLeftIcon />
              ) : (
                <ChevronRightIcon />
              )}
            </IconButton>
          </div>
          <Divider />
          <Listitem/>
        </Drawer>
        <main
          className={clsx(classes.content, {
            [classes.contentShift]: open
          })}
        >
          <div className={classes.drawerHeader} />
          <div>
          <SwitchComponent routes={routes} />
          </div>
        </main>
      </Router>
    </div>
  );
}
