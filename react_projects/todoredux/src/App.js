import React from 'react';
import './App.css';
import NavDrawer from 'Components/DrawerComponent/index';

function App() {
  return (
    <div className="App">
     <NavDrawer/>
    </div>
  );
}

export default App;
