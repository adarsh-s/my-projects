export default function reducer(
  state = {
    tasks: [],
    fetching: false,
    fetched: false,
    done: false,
    open: false,
    error: null
  },
  action
) {
  switch (action.type) {
    case "FETCH_TASK_PENDING": {
      return { ...state, fetching: true, done: false };
    }
    case "FETCH_TASK_REJECTED": {
      return { ...state, fetching: false, error: action.payload };
    }
    case "FETCH_TASK_FULFILLED": {
      return {
        ...state,
        fetching: false,
        fetched: true,
        done: true,
        tasks: action.payload
      };
    }
    case 'DELETE_DATA_FULFILLED': {
      const task = { ...state.tasks }
      const index = state.tasks.data.findIndex(task => task.id === action.payload.data.id)
      task.data.splice(index, 1)
      return {
        ...state,
        done: true,
        fetched: false,
        tasks: task
      }
    }
    case 'EDIT_DATA_FULFILLED': {
      const task = { ...state.tasks }
      const index = task.data.findIndex(task => task.id === action.payload.id.toString())
      task.data[index] = action.payload.res.data
      return {
        ...state,
        done:true,
        fetched:false,
        tasks: task
      }
    }
    case 'SEARCH_FILTER_DATA_FULFILLED': {
      // console.log(JSON.stringify(action.payload))
      return {
        ...state,
        tasks: action.payload.res,
        search: action.payload.data
      }
    }
    default: {
      return {...state};
    }
  }
}
