import React, { useState } from 'react';
import Modal from '@material-ui/core/Modal';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import EditRoundedIcon from '@material-ui/icons/EditRounded';
import useStyles from 'Components/EditFormComponent/styles';
import { editData } from '../Actions/editAction';
import { connect } from "react-redux";

const EditForm=(props)=> {
  const classes = useStyles();
  const [open, setOpen] = useState(false);
  const [title, setTitle] = useState(props.title);
  const [desc, setDesc] = useState(props.desc);
  const handleOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };
  const handleSubmit = () => {
   const tasks={title:title,desc:desc}
   props.dispatch(editData(props.id,tasks))
   handleClose();
  }
  return (
    <div>
      <EditRoundedIcon onClick={handleOpen} />
      <Modal
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
        open={open}
        onClose={handleClose}
      >
        <div className={classes.paper}>
          <h2 id="simple-modal-title">Edit Components</h2>
          <TextField className={classes.textsty} id="title" label="Title" variant="outlined" value={title} onChange={e => setTitle(e.target.value)}/><br /><br />
          <TextField className={classes.textsty} id="description" label="Description" multiline row="4" variant="outlined" value={desc} onChange={e => setDesc(e.target.value)}/><br />
          <Button className={classes.textsty} variant="contained" color="primary" onClick={() => handleSubmit()}>
            Update
            </Button>
        </div>
      </Modal>
    </div>
  );
}
const mapStateToProps = state => {
  return {
    tasks: state.tasks,
    done: state.done
  };
};
export default connect(mapStateToProps)(EditForm);