import React from "react";
import Table from "Views/TodoView/Containers/tableData";
import { connect } from "react-redux";
import { viewTasks } from "Views/TodoView/Actions/viewAction";
import { deleteOneData } from "Views/TodoView/Actions/deleteAction";

class TableData extends React.Component {
  componentDidMount() {
    this.props.dispatch(viewTasks());
  }
 
  deleteHandler = id => {
    const val = window.confirm("Are you sure");
    if (val) {
      this.props.dispatch(deleteOneData(id));
    }
  };

  render() {
    const display =
      this.props.done === true ? (
        <Table
          rows={this.props.tasks.data}
          deletehandler={this.deleteHandler}
          editHandler={this.editHandler}
        />
      ) : (
        ""
      );
    return <div className="App">{display}</div>;
  }
}
const mapStateToProps = state => {
  return {
    tasks: state.tasks,
    done: state.done
  };
};

export default connect(mapStateToProps)(TableData);
