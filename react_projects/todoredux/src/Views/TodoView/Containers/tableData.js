import React from 'react';
import { withStyles, createStyles, makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import EditForm from 'Components/EditFormComponent/index';
import DeleteIcon from '@material-ui/icons/Delete';

const StyledTableCell = withStyles((theme) =>
  createStyles({
    head: {
      backgroundColor: theme.palette.common.black,
      color: theme.palette.common.white,
    },
    body: {
      fontSize: 14,
    },
  }),
)(TableCell);
const useStyles = makeStyles({
  tableco: {
    marginLeft:290,
  },
  table:{
    maxWidth:1000
  }
});
export default function TableView(props) {
  const classes = useStyles();

  return (
    <TableContainer className={classes.tableco} >
      <Table aria-label="customized table" className={classes.table}>
        <TableHead>
          <TableRow>
            <StyledTableCell>Title</StyledTableCell>
            <StyledTableCell>Description</StyledTableCell>
            <StyledTableCell>Edit</StyledTableCell>
            <StyledTableCell>Delete</StyledTableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {props.rows.map(row => (
            <TableRow key={row.id}>
              <StyledTableCell component="th" scope="row">
                {row.title}
              </StyledTableCell>
              <StyledTableCell >{row.desc}</StyledTableCell>
              <StyledTableCell ><EditForm id={row.id} title={row.title} desc={row.desc} /></StyledTableCell>
              <StyledTableCell ><DeleteIcon onClick={()=>props.deletehandler(row.id)}/></StyledTableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}
