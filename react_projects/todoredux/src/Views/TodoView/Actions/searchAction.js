import {get} from 'Utils/apiCallMethod' ;
import apiConstant from "Constants/apiConstant";
export function searchData (data) {
    return dispatch => {
      dispatch({ type: 'SEARCH_FILTER_DATA' })
      get(`${apiConstant.URL}?search=${data}`, data)
        .then(res => {
          dispatch({ type: 'SEARCH_FILTER_DATA_FULFILLED', payload: { res: res, data: data } })
        })
        .catch(err => {
          dispatch({ type: 'SEARCH_FILTER_DATA_REJECTED', payload: err.message })
        })
    }
  }