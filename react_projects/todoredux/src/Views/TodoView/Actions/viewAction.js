import { get } from 'Utils/apiCallMethod'
import apiConstant from 'Constants/apiConstant';
export function viewTasks () {
  return {
    type: 'FETCH_TASK',
    payload: fetchData()
  }
}

async function fetchData () {
  const axiosConfig = {
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'
    }
  }
  const res = await get(`${apiConstant.URL}`, axiosConfig)
  return res;
}
