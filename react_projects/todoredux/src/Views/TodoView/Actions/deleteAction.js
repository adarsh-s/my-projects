import {deleteData} from 'Utils/apiCallMethod'
import apiConstant from 'Constants/apiConstant'
export function deleteOneData (id) {
    return {
      type: 'DELETE_DATA',
      payload: deleteData(`${apiConstant.URL}/${id}`)
    }
}