import apiConstant from "Constants/apiConstant";
import axios from "axios";
export function editData(id, body) {
  return function(dispatch){
    dispatch({type: "EDIT_DATA"});
    axios.put(`${apiConstant.URL}/${id}`, body)
    .then((response)=>{
      dispatch({type:"EDIT_DATA_FULFILLED",payload: {res: response,id:id}})
    }).catch((err)=>{
      dispatch({type: "EDIT_DATA", payload: err})
    })
  };
}
