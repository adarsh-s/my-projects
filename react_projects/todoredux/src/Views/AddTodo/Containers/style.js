

const styles = () => ({
    card: {
      maxWidth: 520,
      maxHeight:600,
      marginTop: 50,
    },
    container: {
      display: "Flex",
      justifyContent: "center"
    },
    actions: {
      float: "right"
    }
  });

  export default styles;