import React from "react";
import { withFormik } from "formik";
import * as Yup from 'yup'
import { withStyles } from "@material-ui/core";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import {addTasks} from 'Views/AddTodo/Action/addAction';
import { connect } from "react-redux";
import styles from 'Views/AddTodo/Containers/style'
const form = props => {
  const {
    classes,
    values,
    touched,
    errors,
    handleChange,
    handleBlur,
    handleSubmit,
  } = props;

  return (
    <div className={classes.container}>
      <form onSubmit={handleSubmit}>
        <Card className={classes.card}>
        <h2>Add Task</h2>
          <CardContent>
            <TextField
              id="title"
              label="Title"
              value={values.title}
              onChange={handleChange}
              onBlur={handleBlur}
              helperText={touched.title ? errors.title : ""}
              error={touched.title && Boolean(errors.title)}
              margin="dense"
              variant="outlined"
              fullWidth
            />
            <TextField
              id="desc"
              label="Description"
              value={values.desc}
              onChange={handleChange}
              onBlur={handleBlur}
              helperText={touched.desc ? errors.desc : ""}
              error={touched.desc && Boolean(errors.desc)}
              margin="dense"
              variant="outlined"
              fullWidth
            />
          </CardContent>
          <Button variant="contained" color="primary" type="submit">
            ADD
          </Button>
        </Card>
      </form>
    </div>
  );
};
const AddForm = withFormik({
  mapPropsToValues: ({
    title,
    desc,
  }) => {
    return {
      title: title || "",
      desc: desc || "",
    };
  },

  validationSchema: Yup.object().shape({
    title: Yup.string().required("Required"),
    desc: Yup.string().required("Required"),
  }),

  handleSubmit: (values, { reset,setSubmitting, props }) => {
   props.dispatch(addTasks(values));
   alert("Task added");
  }
})(form);
const mapStateToProps = state => {
  return {
    tasks: state.tasks,
    done: state.done
  };
};
export default connect(mapStateToProps)(withStyles(styles)(AddForm));