import { post } from "Utils/apiCallMethod";
import apiConstant from "Constants/apiConstant";
export function addTasks(values) {
  return {
    type: "ADD_TASK",
    payload: post( `${apiConstant.URL}`,values)
  };
}

