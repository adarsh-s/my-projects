import React,{Component} from 'react';
import logo from './logo.svg';
import './App.css';
import UserInput from './UserInput/UserInput';
import UserOutput from './UserOutput/UserOutput';

class App extends Component {

  state={
    username:"Max"
  }
  usernameChangedHandler=(event)=>{
    
    this.setState({username: event.target.value});
  }
 render(){
  return (
    <div className="App">
     <h1>Assignment 1</h1>
      <h3>Questions</h3>
      <ol>
        <li>Create two components</li>
        <li>Pass the value using props</li>
        <li>Use State function</li>
        <li>Event Handling</li>
        <li>Two way Binding</li>
        <li>Styling</li>
      </ol>
     <div>
       <UserInput changed={this.usernameChangedHandler } currentName={this.state.username}/>
       <UserOutput userName={this.state.username}/>
       <UserOutput userName="Sam"/>
     </div>
    </div>
    
  );

 }
  }

export default App;
