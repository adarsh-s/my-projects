import React, { Component } from 'react';
import './App.css';
import Validation from './Validation/Validation';
import Char from './Char/Char';
import styled from 'styled-components';

class App extends Component{
  constructor(){
    super();
    this.state={
      userInput:''
    }
  }
  inputChangeHandler=(event)=>{
    this.setState({
      userInput:event.target.value
    });
  }
  
  deleteCharHandler=(index)=>{
    const text=this.state.userInput.split('');
    text.splice(index,1);
    const updatedText=text.join('');
    this.setState({
      userInput:updatedText
    });
  }
  render(){
    const StyledDiv=styled.div`
     width:60%;
     margin:16px auto;
     margin:1px solid;
     box-shadow:0 2px 3px #ccc;
     padding:16px;
     text-align:center;
     
     @media(min-width:500px){
       width:450px;
     }
    `;

    const charList=this.state.userInput.split('').map((ch,index)=>{
      return <Char character={ch} key={index} clicked={()=>this.deleteCharHandler(index)}/>;
    })
    return (
      <div className="App">
        <h1>Assignment 2</h1>
        <h3>Questions</h3>
          <p>1.Validation</p>
          <p>2.CharComponent</p>
          <p>3.List</p>
          <p>4.Styles</p>
          <p>5.Events</p>
          <hr/>
     
        <StyledDiv>
          <input type="text" onChange={this.inputChangeHandler} value={this.state.userInput}/>
          <p>{this.state.userInput}</p>
          <Validation inputLength={this.state.userInput.length}/>
          {charList}
          </StyledDiv>
      </div>
       
    );
  }
  
}

export default App;
