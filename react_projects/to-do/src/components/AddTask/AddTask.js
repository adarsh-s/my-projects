import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';


const useStyles = makeStyles(theme=>({
    div:{
        marginTop:100,
        width:600,
        textAlign:"center",
    },
   
    tablerow:{
        text:'bold'
    },
     root: {
        '& .MuiTextField-root': {
          margin: theme.spacing(1),
          width: 200,
        }
    },
}));

const AddTask=()=>{
    const classes=useStyles()
        return(
           <div className={classes.div}>
              <form className={classes.root}>
                <TextField id="outlined-basic" label="Add Task" variant="outlined" />
                <TextField
                    id="outlined-multiline-static"
                    label="Description"
                    multiline
                    rows="4"
                    variant="outlined"
                    />
              </form>
           </div> 
        );
    }
export default AddTask;