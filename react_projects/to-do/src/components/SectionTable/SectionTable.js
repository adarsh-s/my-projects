import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import EditRoundedIcon from '@material-ui/icons/EditRounded';
import DeleteRoundedIcon from '@material-ui/icons/DeleteRounded';
import VisibilityIcon from '@material-ui/icons/Visibility';


const useStyles = makeStyles({
    div:{
        width:60,
        textAlign:"center",
    },
    table: {
      minWidth: 650,
      textAlign:"center",
      marginTop:100,
    },
    tablerow:{
        text:'bold'
    },
  });
const createData=(title, description)=> {
    return {title,description};
  }

const rows = [
    createData('Task1','description'),
    createData('Task1','description'),
    createData('Task1','description'),
  ];

const SectionTable=()=>{
    const classes=useStyles()
        return(
           <div>
               <TableContainer component={Paper}>
            <Table className={classes.table} aria-label="simple table">
                <TableHead>
                <TableRow>
                    <TableCell>TITLE</TableCell>
                    <TableCell>DESCRIPTION</TableCell>
                    <TableCell>Edit</TableCell>
                    <TableCell>Delete</TableCell>
                    <TableCell>View</TableCell>
                </TableRow>
                </TableHead>
                <TableBody>
                {rows.map((row,index) => (
                    <TableRow key={index}>
                        <TableCell component="th" scope="row">
                            {row.title}</TableCell>
                        <TableCell >{row.description}</TableCell>
                        <TableCell>
                            <EditRoundedIcon  />
                        </TableCell>
                        <TableCell>
                            <DeleteRoundedIcon />
                        </TableCell>
                        <TableCell>
                            <VisibilityIcon  />
                        </TableCell>
                      
                    </TableRow>
                ))}
                </TableBody>
            </Table>
            </TableContainer>
           </div> 
        );
    }
export default SectionTable;