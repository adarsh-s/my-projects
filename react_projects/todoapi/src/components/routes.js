import ListTable from 'components/TableList/TableList'
import AddTask from 'components/AddTask/AddTask';
const routes = [
    {
      path: "/",
      exact: true,
      component: ListTable
    },
    {
      path: "/AddTask",
      component: AddTask
    },
  ]

export default routes;