import React from 'react';
import InputBase from '@material-ui/core/InputBase';
import useStyles from 'components/Drawer/drawerStyles'
const Searchbar=(props)=>{
    const classes = useStyles();
    return(
        <div className={classes.search}>
    <InputBase
      placeholder="Search…"
      classes={{
        root: classes.inputRoot,
        input: classes.inputInput,
      }}
      onChange={()=>props.searchVal()}
      inputProps={{ 'aria-label': 'search' }}
    />
  </div>
    );
}

export default Searchbar