import React, { useState } from 'react';
import Modal from '@material-ui/core/Modal';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import EditRoundedIcon from '@material-ui/icons/EditRounded';
import variables from 'constants/constants';
import useStyles from './EditStyle';

export default function EditForm(props) {
  const classes = useStyles();
  const [open, setOpen] = useState(false);
  const [title, setTitle] = useState(props.title);
  const [description, setDescription] = useState(props.description);
  const handleOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };
  const handleSubmit = (event) => {
    fetch(`${variables.eachObjUrl}${props.id}`, {
      method: 'PUT',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        Title: title,
        Description: description
      })
    }).then(res => res.json())
    handleClose();

  }
  return (
    <div>
      <EditRoundedIcon onClick={handleOpen} />
      <Modal
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
        open={open}
        onClose={handleClose}
      >
        <div className={classes.paper}>
          <h2 id="simple-modal-title">Edit Components</h2>
          <TextField className={classes.textsty} id="title" label="Title" variant="outlined" value={title} onChange={e => setTitle(e.target.value)} /><br /><br />
          <TextField className={classes.textsty} id="description" label="Description" multiline row="4" variant="outlined" onChange={e => setDescription(e.target.value)} value={description} /><br />
          <Button className={classes.textsty} variant="contained" color="primary" onClick={() => handleSubmit()}>
            Update
            </Button>
        </div>
      </Modal>
    </div>
  );
}