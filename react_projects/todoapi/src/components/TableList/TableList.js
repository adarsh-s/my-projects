import React, { Component } from 'react';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import DeleteRoundedIcon from '@material-ui/icons/DeleteRounded';
import EditForm from '../EditForm/EditForm';
import variables from 'constants/constants';
import Snackbar from '@material-ui/core/Snackbar';
import Description from 'components/DescriptionView/Description'

const StyledTableCell = withStyles(theme => ({
  head: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,  
  },
}))(TableCell);
const StyledTableRow = withStyles(theme => ({
  root: {
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.background.default,
      whiteSpace: 'normal',
      wordWrap: 'break-word'
    },
  },
}))(TableRow);
const classes = makeStyles();
class ListTable extends Component {
  constructor(props) {
    super(props);
    this.state = {
      task: [],
    }
  }
  componentDidMount() {
    fetch(`${variables.baseUrl}`)
      .then(res => res.json())
      .then((data) => {
        this.setState({ task: data })
      })
      .catch(console.log)
  }
  componentDidUpdate(){
    fetch(`${variables.baseUrl}`)
      .then(res => res.json())
      .then((data) => {
        this.setState({ task: data })
      })
      .catch(console.log)
  }
  deleteHandler(id) {
    let r = window.confirm("Are you sure?");
    if (r === true) {
      fetch(`${variables.eachObjUrl}${id}`, {
        method: `${variables.delete}`
      }).then(this.setState({ open: true }))
        .then(response =>
          response.json().then(json => {
            return json;
          }).then(fetch(`${variables.baseUrl}`)
            .then(res => res.json())
            .then((data) => {
              this.setState({ task: data })
            }))
        );
    }
  }
  render() {
    return (
      <React.Fragment>
        <TableContainer>
          <Table className={classes.table} aria-label="customized table">
            <TableHead>
              <TableRow>
                <StyledTableCell>Title</StyledTableCell>
                <StyledTableCell>Descriptions</StyledTableCell>
                <StyledTableCell>Edit</StyledTableCell>
                <StyledTableCell>Delete</StyledTableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {this.state.task.map((todo) => (
                <StyledTableRow key={todo.id}>
                  <StyledTableCell component="th" scope="row">
                    <Description title={todo.Title} description={todo.Description}/>
                  </StyledTableCell>
                  <StyledTableCell ><span style={{ whiteSpace: 'nowrap',textOverflow:'ellipsis' , overflow: 'hidden', width: '150px', display: 'block' }}>{todo.Description}</span></StyledTableCell>
                  <StyledTableCell ><EditForm id={todo.id} title={todo.Title} description={todo.Description} /></StyledTableCell>
                  <StyledTableCell ><DeleteRoundedIcon onClick={() => this.deleteHandler(todo.id)} /></StyledTableCell>
                </StyledTableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
        <Snackbar open={this.state.open} autoHideDuration={1000} message="Deleted" />
      </React.Fragment>
    );
  }
}
export default ListTable
