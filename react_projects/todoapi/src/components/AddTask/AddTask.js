import React from 'react';
import { Formik } from 'formik';
import Button from '@material-ui/core/Button';
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator';
import variables from 'constants/constants';

export default class SimpleFormExample extends React.Component {
    state = {
        formData: {
            Title: '',
            Description: '',
        },
        submitted: false,
    }

    handleChange = (event) => {
        const { formData } = this.state;
        formData[event.target.name] = event.target.value;
        this.setState({ formData });
    }

    handleSubmit = () => {
        this.setState({ submitted: true }, () => {
            fetch(`${variables.baseUrl}`, {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    Title: this.state.formData.Title,
                    Description: this.state.formData.Description
                })
            })
                .then(res => res.json())
    setTimeout(() => this.setState({ submitted: false }), 5000);
        });
        this.setState({
            formData:{
                Title: '',
                Description: '',
            }
        })

    }

render() {
    const { formData, submitted } = this.state;
    return (
        <ValidatorForm
            ref="form"
            onSubmit={this.handleSubmit}
        >
            <h2>Add New Task</h2>
            <TextValidator
                label="Title"
                onChange={this.handleChange}
                name="Title"
                variant="outlined"
                value={formData.Title}
                validators={['required']}
                errorMessages={['this field is required']}
            />
            <br /><br />
            <TextValidator
                label="Description"
                onChange={this.handleChange}
                multiline
                rows="4"
                name="Description"
                variant="outlined"
                value={formData.Description}
                validators={['required']}
                errorMessages={['this field is required']}
            />
            <br /><br />
            <Button
                color="primary"
                variant="contained"
                type="submit"
                disabled={submitted}
            >
                {
                    (submitted && 'Task Added')
                    || (!submitted && 'Add Task')
                }
            </Button>
        </ValidatorForm>
    );
}
}