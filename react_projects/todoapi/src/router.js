import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import ListAltIcon from '@material-ui/icons/ListAlt';
import AddCircleIcon from '@material-ui/icons/AddCircle';
import List from '@material-ui/core/List'
import useStyles from 'components/Drawer/drawerStyles'
import React from 'react';
import {
    NavLink
  } from "react-router-dom";
  
const Listitem=()=>{
    const classes = useStyles();
    return(
        <div>
            <List>
          <ListItem button component={NavLink} to="/" activeClassName="Mui-selected" exact>
            <ListItemIcon>
              <ListAltIcon/>
            </ListItemIcon>
            <ListItemText primary="TaskList" />
          </ListItem>
          <ListItem button component={NavLink} to="/AddTask" activeClassName="Mui-selected" exact> 
            <ListItemIcon>
              <AddCircleIcon/>
            </ListItemIcon>
            <ListItemText primary="Add Task" />
          </ListItem >
          </List>
        </div>
    );
}
export default Listitem;