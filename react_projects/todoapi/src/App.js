import React, { Component } from 'react';
import './App.css';
import AddView from 'views/AddView';

class App extends Component {
  render() {
    return (
      <div className="App">
        <AddView />
      </div>
    );
  }
}

export default App;
