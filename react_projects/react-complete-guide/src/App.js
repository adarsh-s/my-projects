import React, { useState } from 'react';
import logo from './logo.svg';
import './App.css';
import Person from './Person/Person';

const App =props=> {
  const [personState,setPersonState]=useState({
    persons:[
      {name:"Manu",age:12},
      {name:"Sam",age:22},
      {name:"Ram",age:32},
    ]  
  });
  const switchNameHandler=(newname)=>{
    
    setPersonState({
      persons:[
        {name:newname,age:18},
        {name:"Sam",age:22},
       {name:"Ram",age:32},
      ]
    });
  }; 
  const nameChangeHandler=(event)=>{
    
    setPersonState({
      persons:[
        {name:"Damu",age:18},
        {name:event.target.value,age:22},
       {name:"Ram",age:32},
      ]
    });
  }; 


  return (
    <div className="App">
      <h1>React App</h1>
      <button onClick={switchNameHandler.bind(this,"Damu")}>Switch Name</button>
      <Person name={personState.persons[0].name} age={personState.persons[0].age} changed={nameChangeHandler}/>      
      <Person name={personState.persons[1].name} age={personState.persons[1].age} click={switchNameHandler.bind(this,"Damu!")}/>      
      <Person name={personState.persons[2].name} age={personState.persons[2].age}/>      
    </div>
  );

}  
    


export default App;
